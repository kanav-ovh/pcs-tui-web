package com.oyo.pcstuiweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class PcsTuiWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(PcsTuiWebApplication.class, args);
	}
}
